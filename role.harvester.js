var resourceManager = require('resource.manager');
var roleHarvester = {

    /** @param {Creep} creep **/
    run: function(creep) {
        /*if(creep.memory.moveToPos){
            if(!creep.pos.isNearTo(creep.memory.moveToPos)){
                creep.moveTo(creep.memory.moveToPos, {visualizePathStyle: {stroke: '#ffffff'}});
            }
            else if(creep.carry.energy < creep.carryCapacity && creep.harvest(creep.memory.moveToPos.look()) == 'OK'){

            }
        }*/
        //console.log(creep.name, 'Tese: ' + creep.carry.energy < creep.carryCapacity);
	    if((creep.memory.workPart && creep.store[RESOURCE_ENERGY] + creep.memory.workPart*2  <= creep.store.getCapacity()) || 
           (!creep.memory.workPart && creep.store[RESOURCE_ENERGY] < creep.store.getCapacity())) {
	        //creep.say('🔄 harvest');
	        if(!creep.memory.target){
                var tar = creep.pos.findClosestByRange(FIND_SOURCES, {
                    filter: (source) => {
                        return (resourceManager.getEstimate(source.id, source.room.name) > 0) ;
                    }
                });
                
                if(tar){
                    creep.memory.target = tar.id;
                }
	            //creep.say(source.pos.x, source.pos.y);
	        }
            var source = null;
            if(creep.memory.target)
            {
                source = Game.getObjectById(creep.memory.target);
            }
            if(source)
            {
                if (!creep.memory.workPart)
                {
                    var workPart = 0;
                    for(var idx in creep.body)
                    {
                        var part = creep.body[idx].type;
                        if (part == WORK)
                        {
                            workPart += 1;
                        }
                    }
                    creep.memory.workPart = workPart
                }
                //console.log("workPart: ",workPart);
                resourceManager.changeEstimate(source.id,-creep.memory.workPart*2,source.room.name);
                if(source.room == creep.room && creep.harvest(source) == ERR_NOT_IN_RANGE || !creep.pos.isNearTo(source)) {
                    //creep.memory.moveToPos = source.pos;
                    creep.moveTo(source, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms:1, ignoreCreeps: false});
                }
            }
        }
        else {
            //creep.say('⚡ unload');
            var target = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType == STRUCTURE_LINK &&
                                resourceManager.getEstimate(structure.id) < structure.energyCapacity &&
                                creep.pos.inRangeTo(structure, 3)) ;
                    }
            });
            if(!target){
                target = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                        filter: (structure) => {
                            return ((structure.structureType == STRUCTURE_EXTENSION ||
                                    structure.structureType == STRUCTURE_SPAWN ||
                                    structure.structureType == STRUCTURE_TOWER ||
                                    structure.structureType == STRUCTURE_CONTAINER ||
                                    structure.structureType == STRUCTURE_TERMINAL) &&
                                    resourceManager.getEstimate(structure.id) < structure.store.getCapacity(RESOURCE_ENERGY) &&
                                    creep.pos.inRangeTo(structure, 3)) ;
                        }
                });
            }
            if(!target){
                target = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                        filter: (structure) => {
                            return ((structure.structureType == STRUCTURE_EXTENSION ||
                                    structure.structureType == STRUCTURE_SPAWN ||
                                    structure.structureType == STRUCTURE_TOWER ||
                                    structure.structureType == STRUCTURE_CONTAINER ||
                                    structure.structureType == STRUCTURE_TERMINAL) &&
                                    resourceManager.getEstimate(structure.id) < structure.store.getCapacity(RESOURCE_ENERGY)) ;
                        }
                });
            }
            //console.log(creep.name, 'T: ' + target);
            if(target) {
                if(creep.transfer(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    //creep.memory.moveToPos = targets[0].pos;
                    creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms:1, ignoreCreeps: false});
                }
            }
        }
        /*else {
            //creep.say('⚡ unload');
            if(creep.memory.destination){
                var destination = Game.getObjectById(creep.memory.destination);
            }
            else{
                var destination = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                    filter: (structure) => {return structure.structureType == STRUCTURE_STORAGE;}});
                if(destination){
                    creep.memory.destination = destination.id;
                }
            }
            if(destination && creep.transfer(destination, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE{
                //creep.memory.moveToPos = targets[0].pos;
                creep.moveTo(destination, {visualizePathStyle: {stroke: '#ffffff'}});
            }
        }*/
        if(creep.room.name != creep.memory.resetPos.roomName){
            creep.moveTo(creep.memory.resetPos, {visualizePathStyle: {stroke: '#ffffff'}, ignoreCreeps: true});
        }
	}
};

module.exports = roleHarvester;
