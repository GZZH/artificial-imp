var roleHealer = require('role.healer');
var roleReserver = require('role.reserver');
var roleLongDisHarvester = require('role.longDisHarvester');

var roleCloserange = require('role.closerange')
var roleLongrange = require('role.longrange')

var warband = function warband(num, meetPoint, attackPoint){
    /** @param {Creep} creep **/
    this.warband_num = num;
    this.meetPoint = meetPoint;
    this.attackPoint = attackPoint;
    this.num = function(){
        return this.warband_num;
    };
    this.name =function(){
        return 'Warband' + this.warband_num;
    };
    this.isNeeded = function(creepType, num){
        var warband = Memory.Warbands[this.name()];
        if(!warband){
            return false;
        }
        var composition = warband.Composition;
        for(var ct in composition){
            if(ct == creepType && num < composition[creepType]){
                return true;
            }
        }
        return false;
    };
    this.findFlag = function(flagName){
        if(flagName && flagName in Game.flags){
            return Game.flags[flagName];
        }
        return null;
    };
    this.run = function(creeps) {
        var attackFlag = this.findFlag(this.attackPoint);
        var meetFlag = this.findFlag(this.meetPoint);
        //console.log(this.name(), attackFlag)
	    for(var idx in creeps){
	        var creep = creeps[idx];
	        if(attackFlag){
                if(attackFlag.room == creep.room){
	                var closestHostile = attackFlag.pos.findClosestByPath(FIND_HOSTILE_CREEPS, {
                        filter: (hostile) => {return (attackFlag.pos.inRangeTo(hostile, 10) && Memory.WhiteList.indexOf(hostile.owner.username) == -1 );}
                    });
	                var closestStructure = attackFlag.pos.findClosestByPath(FIND_HOSTILE_STRUCTURES, {
                        filter: (structure) => {return (structure.structureType == STRUCTURE_TOWER);}
                    });
                    if (!closestStructure){
                        closestStructure = attackFlag.pos.findClosestByPath(FIND_HOSTILE_STRUCTURES, {
                            filter: (structure) => {return (structure.structureType != STRUCTURE_CONTROLLER);}
                        })
                    }
                }
                else{
                    var closestHostile = null;
                    var closestStructure = null;
                }
	        }
	        if(creep.memory.role == 'closeRange'){
	            if(attackFlag){
                    if(closestHostile){
                        roleCloserange.attack(creep,closestHostile);
                    }
                    else if(closestStructure){
                        roleCloserange.attack(creep,closestStructure);
                    }
                    else{
                        if(!roleCloserange.sentinel(creep))
                        {
        	                roleCloserange.goto(creep,attackFlag);
                        }
                    }
	            }
	            else if(meetFlag){
	                if(!roleCloserange.sentinel(creep))
                    {
    	                roleCloserange.goto(creep,meetFlag);
                    }
	            }
            }
            if(creep.memory.role == 'longRange'){
                if(attackFlag){
	                if(closestHostile){
	                    roleLongrange.attack(creep,closestHostile);
	                }
	                else if(closestStructure){
	                    roleLongrange.attack(creep,closestStructure);
	                }
	                else{
	                    if(!roleLongrange.sentinel(creep))
                        {
        	                roleLongrange.goto(creep,attackFlag);
                        }
	                }
	            }
	            else if(meetFlag){
	                if(meetFlag.room != creep.room || !roleLongrange.sentinel(creep))
                    {
    	                roleLongrange.goto(creep,meetFlag);
                    }
	            }
            }
            if(creep.memory.role == 'healer'){
                var target = creep.pos.findClosestByRange(FIND_CREEPS, {
                    filter: (dammaged_creep) => dammaged_creep.my && dammaged_creep.hits < dammaged_creep.hitsMax
                });
                if (target){
                    roleHealer.heal(creep,target);
                }
                else if(attackFlag){
	                if(!roleHealer.sentinel(creep))
                    {
    	                roleHealer.goto(creep,attackFlag);
                    }
	            }
	            else if(meetFlag){
	                if(!roleHealer.sentinel(creep))
                    {
    	                roleHealer.goto(creep,meetFlag);
                    }
	            }
            }
            if(creep.memory.role == 'reserver'){
                if(attackFlag){
                    if(!roleReserver.reserve(creep, attackFlag.pos, 10))
                    {
    	                roleReserver.goto(creep,attackFlag);
                    }
	            }
	            else if(meetFlag){
	                if(!roleReserver.sentinel(creep))
                    {
    	                roleReserver.goto(creep,meetFlag);
                    }
	            }
            }
            if(creep.memory.role == 'longDisHarvester'){
                roleLongDisHarvester.state(creep);
                //console.log(creep)
                if(roleLongDisHarvester.scavenge(creep, attackFlag)){
                    //console.log(creep)
                }
                else if(creep.memory.harvesting){
                    if(!attackFlag){
                        if(creep.room != Game.spawns['Spawn1'].room){
	                        roleLongDisHarvester.goto(creep, Game.spawns['Spawn1'].pos);
                        }
                        else{
                            roleLongDisHarvester.changeJob(creep);
                        }
                    }
                    else if(!roleLongDisHarvester.harvest(creep, attackFlag)){
                        roleLongDisHarvester.goto(creep, attackFlag.pos);
                    }
	            }
	            else {
                    //console.log(creep.name, roleLongDisHarvester.unload(creep, meetFlag))
                    if(attackFlag && !roleLongDisHarvester.build(creep,attackFlag))
                    {
                        if(!meetFlag){
        	                if(creep.room != Game.spawns['Spawn1'].room){
                                roleLongDisHarvester.goto(creep, Game.spawns['Spawn1'].pos);
                            }
                            else{
                                roleLongDisHarvester.changeJob(creep);
                            }
    	                }

                        else if(!roleLongDisHarvester.unload(creep, meetFlag)){
                            roleLongDisHarvester.goto(creep, meetFlag.pos);
                        }
                    }
	            }
            }
	    }
	};
};

module.exports = warband;
