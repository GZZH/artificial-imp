var roleReserver = {
    /** @param {Creep} creep **/
	goto: function(creep, direction) {
	    if(direction){
	        creep.moveTo(direction, {visualizePathStyle: {stroke: '#ffffff'}, ignoreCreeps: false})
	    }
	},
	reserve: function(creep, pos, range = 10) {
	    if(pos && creep.room.controller && pos.inRangeTo(creep.room.controller, 10)){
	        if(!creep.room.controller.owner && (!creep.room.controller.reservation || creep.room.controller.reservation.username == creep.owner.username)){
				if(creep.reserveController(creep.room.controller) == ERR_NOT_IN_RANGE || !creep.pos.isNearTo(creep.room.controller)) {
                        creep.moveTo(creep.room.controller, {visualizePathStyle: {stroke: '#ffffff'}});
                }
	        }
	        else if(!creep.room.controller.my || (creep.room.controller.reservation && creep.room.controller.reservation.username != creep.owner.name)){
	            if(creep.attackController(creep.room.controller) == ERR_NOT_IN_RANGE || !creep.pos.isNearTo(creep.room.controller)) {
                        creep.moveTo(creep.room.controller, {visualizePathStyle: {stroke: '#ffffff'}});
                }
	        }
            return true
	    }
	    return false
	},
	sentinel: function(creep) {
        if(creep.room.controller && !creep.room.controller.my){
            if (creep.reserveController(creep.room.controller) == ERR_NOT_IN_RANGE || creep.attackController(creep.room.controller) == ERR_NOT_IN_RANGE || !creep.pos.isNearTo(creep.room.controller)){
                creep.moveTo(creep.room.controller, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms:1});
            }
            return true
	    }
	    return false
	}
};

module.exports = roleReserver;
