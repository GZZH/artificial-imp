var roleFixer = require('role.fixer');
var resourceManager = require('resource.manager');

var roleBuilder = {
    /** @param {Creep} creep **/
    run: function(creep) {
	    if(creep.memory.building && creep.carry.energy == 0) {
            creep.memory.building = false;
            creep.say('🔄 charge');
	    }

	    if(!creep.memory.building && creep.carry.energy == creep.carryCapacity) {
	        creep.memory.building = true;
	        creep.say('🚧 build');
	    }

	    if(creep.memory.building) {
	        var target = creep.pos.findClosestByRange(FIND_CONSTRUCTION_SITES,{
                    filter: (structure) => {return structure.my;}});
            if(target) {
                if(creep.build(target) == ERR_NOT_IN_RANGE|| !creep.pos.isNearTo(target)) {
                    creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms: 1});
                }
            }
            else{
                roleFixer.run(creep);
            }
	    }
	    else {
	        var supply = null;
            //console.log('--------------------------\n')
            for(var idx in Memory.ProduceContainersID){
                var cache_target = Game.getObjectById(Memory.ProduceContainersID[idx]);
                //console.log(creep.name, ': ',  cache_target, cache_target.store[RESOURCE_ENERGY]);
                if(cache_target && resourceManager.getEstimate(cache_target.id) > 0 &&
                  (supply == null ||
                   creep.pos.getRangeTo(supply) > creep.pos.getRangeTo(cache_target))){
                    supply = cache_target;
                    //console.log(creep.name, 'change to: ',  cache_target);
                }
                if(cache_target && resourceManager.getEstimate(cache_target.id) >= cache_target.storeCapacity){
                    supply = cache_target;
                    break;
                }
            }
            if(!supply){
                supply = creep.room.storage;
            }
            if(!supply)
            {
                supply = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                    filter: (structure) => {return ((structure.structureType == STRUCTURE_CONTAINER || structure.structureType == STRUCTURE_TERMINAL )&&
                                                    !Memory.SecondaryContainersID.includes(structure.id) &&
                                                    resourceManager.getEstimate(structure.id) > 0);
                    }
                });
            }
            //console.log(creep.name, 'final: ',  supply, supply.store[RESOURCE_ENERGY]);
            //console.log(creep.name, 'load from: ', destination);
            if(supply){
                resourceManager.changeEstimate(supply.id, -(creep.carryCapacity - creep.carry.energy), supply.room.name);
                if(creep.withdraw(supply,RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(supply, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms:1})
                }
            }
	    }

        if(creep.room.name != creep.memory.resetPos.roomName){
            creep.moveTo(creep.memory.resetPos, {visualizePathStyle: {stroke: '#ffffff'}, ignoreCreeps: true});
        }
	}
};

module.exports = roleBuilder;
