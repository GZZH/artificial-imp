var resourceManager = {
    update: function() {
        var cache_estimate_remain = {};
        for(var roomidx in Game.rooms){
            var room = Game.rooms[roomidx]
            cache_estimate_remain[room.name] = {}
            var structures = room.find(FIND_STRUCTURES, {
                filter: (structure) => {return (structure.store);
                }
            });
            var sources = room.find(FIND_SOURCES);
            for(var idx in structures){
                var target = structures[idx];
                cache_estimate_remain[room.name][target.id] = target.store[RESOURCE_ENERGY];
            }
            for(var idx in sources){
                var target = sources[idx];
                cache_estimate_remain[room.name][target.id] = 14;
            }
        }
        Memory.estimate_remain = cache_estimate_remain;
    },
    changeEstimate: function(id, amount, room) {
        Memory.estimate_remain[room][id] = Memory.estimate_remain[room][id] + amount;
    },
    setEstimate: function(id, amount, room) {
        Memory.estimate_remain[room][id] = amount;
    },
    getEstimate: function(id, room = null) {
        if(room)
        {
            if(id in Memory.estimate_remain[room]){
                return Memory.estimate_remain[room][id];
            }
            else
            {
                return 0
            }
        }
        for(var room in Memory.estimate_remain)
        {
            if(id in Memory.estimate_remain[room]){
                return Memory.estimate_remain[room][id];
            }
        }
        return 0;
    },
    printAll: function(room = null) {
        if(room){
            for(var idx in Memory.estimate_remain[room]){
                console.log(idx, ': ', Memory.estimate_remain[room][idx])
            }
        }
        else{
            for(var room in Memory.estimate_remain)
            {
                for(var idx in Memory.estimate_remain[room]){
                    console.log(idx, ': ', Memory.estimate_remain[room][idx])
                }
            }
        }
    }
};

module.exports = resourceManager;
