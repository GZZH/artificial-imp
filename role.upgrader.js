var roleHarvester = require('role.harvester');
var resourceManager = require('resource.manager');

var roleUpgrader = {
    /** @param {Creep} creep **/
    run: function(creep) {
        if(creep.memory.upgrading && creep.carry.energy == 0) {
            creep.memory.upgrading = false;
            creep.say('🔄 charge');
	    }
	    else if(!creep.memory.upgrading && creep.carry.energy == creep.carryCapacity) {
	        creep.memory.upgrading = true;
	        creep.say('⚡ upgrade');
	    }

	    if(creep.memory.upgrading) {
	        if(creep.room.controller.my && (creep.room.controller.level != 8 ||creep.room.controller.ticksToDowngrade < 200000 - 15)){
                if(creep.upgradeController(creep.room.controller) == ERR_NOT_IN_RANGE || !creep.pos.isNearTo(creep.room.controller)) {
                    creep.moveTo(creep.room.controller, {visualizePathStyle: {stroke: '#ffffff'},
                                                         maxRooms: 1});
                }
	        }
	        else{
	            creep.moveTo(creep.memory.resetPos, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms:1, ignoreCreeps: true});
	        }
        }
        else {

            var target = creep.room.controller.pos.findClosestByRange(FIND_MY_STRUCTURES, {filter: (structure) => {return structure.structureType == STRUCTURE_LINK &&
                                                                                                                          structure.pos.inRangeTo(creep.room.controller.pos, 3);}});
            if(!target){
                target = creep.room.storage;
            }
            if(!target){
                target = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType == STRUCTURE_CONTAINER &&
                            structure.store[RESOURCE_ENERGY] > 0);
                        }});
            }
            //console.log('upgrader: ', creep.name, target);
            if(target) {
                resourceManager.changeEstimate(target.id, -(creep.carryCapacity - creep.carry.energy), target.room.name);
                if(creep.withdraw(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    //creep.memory.moveToPos = targets[0].pos;
                    /*if(creep.pos.inRangeTo(creep.room.controller, 5) || true){
                        creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms:1, ignoreCreeps: !creep.pos.inRangeTo(creep.room.controller, 5)});
                    }*/
                    //else{
                    creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms:1});
                    //}
                }
            }
            else{
                creep.moveTo(creep.room.controller, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms:1, ignoreCreeps: true});
            }
        }
        if(creep.room.name != creep.memory.resetPos.roomName){
            creep.moveTo(creep.memory.resetPos, {visualizePathStyle: {stroke: '#ffffff'}, ignoreCreeps: true});
        }
	}
};

module.exports = roleUpgrader;
