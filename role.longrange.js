var roleLongrange = {
    /** @param {Creep} creep **/
	goto: function(creep, direction) {
	    if(direction){
	        creep.moveTo(direction, {visualizePathStyle: {stroke: '#ffffff'}})
	    }
	},
	attack: function(creep, target) {
		console.log('fuckyou',target)
	    if(target){
	        if(creep.rangedAttack(target) == ERR_NOT_IN_RANGE) {
                creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}});
            }
            if(creep.pos.getRangeTo(target) < 3){
                const direction = -creep.pos.getDirectionTo(target);
                creep.move(direction);
            }
	    }
	},
	sentinel: function(creep) {
	    var closestHostile = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS, {
                                filter: (hostile) => {return (hostile.body.includes(HEAL) && Memory.WhiteList.indexOf(hostile.owner.username) == -1);}
                            })
        if(!closestHostile){
            closestHostile = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS,{
                        filter: (hostile) => {return (Memory.WhiteList.indexOf(hostile.owner.username) == -1 );}})
        }
	    var closestStructure = creep.pos.findClosestByPath(FIND_HOSTILE_STRUCTURES, {
                                filter: (structure) => {return (structure.structureType != STRUCTURE_CONTROLLER);}
                            })
        if(closestHostile) {
            if(creep.rangedAttack(closestHostile) == ERR_NOT_IN_RANGE || !creep.pos.isNearTo(closestHostile)) {
                    creep.moveTo(closestHostile, {visualizePathStyle: {stroke: '#ffffff'}});
            }
            return true
        }
        else if(closestStructure) {
            if(creep.rangedAttack(closestStructure) == ERR_NOT_IN_RANGE || !creep.pos.isNearTo(closestStructure)) {
                    creep.moveTo(closestStructure, {visualizePathStyle: {stroke: '#ffffff'}});
            }
            return true
        }
        else{
            return false
        }
	}
};

module.exports = roleLongrange;
