var resourceManager = require('resource.manager');
var roleCarryer = require('role.carryer');

var roleCharger = {
    /** @param {Creep} creep **/
    run: function(creep) {
        //console.log(creep.name, ' Time: ', Game.time);
        var storage = creep.room.storage;
        var storageLink = null;
        if(storage){
            storageLink = storage.pos.findClosestByRange(FIND_STRUCTURES, {filter: (structure) => {return structure.structureType == STRUCTURE_LINK;}});
            var free = (!storageLink || resourceManager.getEstimate(storageLink.id,storageLink.room.name) <= 0) && (storage && resourceManager.getEstimate(storage.id,storage.room.name) > 10000)
            if(storageLink)
            {
                console.log(creep.room,'main link energy: ',resourceManager.getEstimate(storageLink.id,storageLink.room.name));
            }

            if(creep.memory.unloading && creep.carry.energy == 0) {
                creep.memory.unloading = false;
                //creep.say('🔄 load');
    	    }
    	    if(!creep.memory.unloading && creep.carry.energy > 0) {
    	        creep.memory.unloading = true;
    	        //creep.say('🔄 unload');
    	    }
            if(creep.memory.unloading) {
                var destination = storage;
                if(free || _.sum(storage.store) >= storage.store.getCapacity()){
                    var targets = creep.pos.findInRange(FIND_STRUCTURES, 1, {filter: (structure) => {return structure.structureType == STRUCTURE_TOWER &&
                                                                                                            resourceManager.getEstimate(structure.id,structure.room.name) < structure.store.getCapacity(RESOURCE_ENERGY);}});

                    if(!targets.length)
                    {
                        targets = creep.pos.findInRange(FIND_STRUCTURES, 1, {filter: (structure) => {return structure.structureType != STRUCTURE_STORAGE &&
                            structure.structureType != STRUCTURE_LINK &&
                            structure.store &&
                            resourceManager.getEstimate(structure.id,structure.room.name) < structure.store.getCapacity(RESOURCE_ENERGY);}});
                            var lowestEnergy = null
                    }
                    for(var idx in targets){
                        var target = targets[idx]
                        var targerEnergy = resourceManager.getEstimate(target.id,target.room.name) / target.store.getCapacity(RESOURCE_ENERGY);
                        if(!lowestEnergy ||  targerEnergy < lowestEnergy){
                            lowestEnergy = targerEnergy;
                            destination = target;
                        }
                    }
                }
                if(destination){
                    resourceManager.changeEstimate(destination.id, creep.carry.energy, destination.room.name);
                    if(creep.transfer(destination, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE){
                        creep.moveTo(destination, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms:1, ignoreCreeps: !creep.pos.inRangeTo(destination, 5)});
                    }
                }
            }
            else {
                supply = storageLink;
                if(supply){
                    resourceManager.changeEstimate(supply.id, -(creep.carryCapacity - creep.carry.energy), supply.room.name);
                    if(creep.withdraw(supply,RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(supply, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms:1, ignoreCreeps: !creep.pos.inRangeTo(supply, 5)})
                    }
                }
            }
        }
        if(creep.room.name != creep.memory.resetPos.roomName){
            creep.moveTo(creep.memory.resetPos, {visualizePathStyle: {stroke: '#ffffff'}, ignoreCreeps: true});
        }
	}
};

module.exports = roleCharger;
