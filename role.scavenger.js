var roleCarryer = require('role.carryer');
var resourceManager = require('resource.manager');

var roleScavenger = {

    /** @param {Creep} creep **/
    run: function(creep) {

	    if(!creep.memory.scavenging && _.sum(creep.carry) == 0) {
            creep.memory.scavenging = true;
            creep.say('🔄 picking');
	    }
	    else if(creep.memory.scavenging && _.sum(creep.carry) == creep.carryCapacity && !creep.memory.carrying) {
	        creep.memory.scavenging = false;
	        creep.say('🔄 unload');
	    }

	    if(creep.memory.scavenging) {
	        var target = creep.pos.findClosestByRange(FIND_DROPPED_RESOURCES);
	        var tombstone = creep.pos.findClosestByRange(FIND_TOMBSTONES, {
                filter: (tombstone) => _.sum(tombstone.store) > 0
            });
            var ruin = creep.pos.findClosestByRange(FIND_RUINS, {
                filter: (ruin) => _.sum(ruin.store) > 0
            });
            if(target) {
                creep.memory.carrying = false;
                if(creep.pickup(target) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms:1, ignoreCreeps: false && !creep.pos.inRangeTo(target, 3)});
                }
            }
            else if(tombstone){
                creep.memory.carrying = false;
                var key = null;
                for(var k in tombstone.store){
                    if(!key && tombstone.store[k] > 0){
                        key = k;
                    }
                }
                if(key && creep.withdraw(tombstone, key) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(tombstone, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms:1, ignoreCreeps: false && !creep.pos.inRangeTo(tombstone, 3)});
                }
            }
            else if(ruin){
                creep.memory.carrying = false;
                var key = null;
                for(var k in ruin.store){
                    if(!key && ruin.store[k] > 0){
                        key = k;
                    }
                }
                if(key && creep.withdraw(ruin, key) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(ruin, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms:1, ignoreCreeps: false && !creep.pos.inRangeTo(ruin, 3)});
                }
            }
            else{
                /*if(creep.carry.energy > 0 && !creep.memory.carrying){
                    this.unload(creep);
                }
                else{
                    creep.memory.carrying = true;
                    roleCarryer.run(creep);
                }*/
                creep.memory.carrying = true;
                roleCarryer.run(creep);
            }
	    }
	    else {
	        this.unload(creep);
	    }
        if(creep.room.name != creep.memory.resetPos.roomName){
            creep.moveTo(creep.memory.resetPos, {visualizePathStyle: {stroke: '#ffffff'}, ignoreCreeps: true});
        }
	},
	unload: function(creep) {
        if(_.sum(creep.carry) != creep.carry.energy && creep.room.storage){
            var target = creep.room.storage
        }
        else{
            var target = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                filter: (structure) => {
                    return ((((structure.structureType == STRUCTURE_EXTENSION ||
                        structure.structureType == STRUCTURE_SPAWN) &&
                        structure.energy < structure.energyCapacity)||
                        ((structure.structureType == STRUCTURE_CONTAINER ||
                            structure.structureType == STRUCTURE_STORAGE) &&
                            _.sum(structure.store) < structure.storeCapacity))) ;
                        }
                    });
        }
        //console.log(creep.name, 'T: ' + target);
        if(target) {
            var key = null;
            for(var k in creep.carry){
                if(creep.carry[k] > 0){
                    key = k;
                }
            }
            if(key == RESOURCE_ENERGY){
                resourceManager.changeEstimate(target.id, creep.carry.energy, target.room.name);
            }
            if(creep.transfer(target, key) == ERR_NOT_IN_RANGE || !creep.pos.isNearTo(target)) {
                creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms:1, ignoreCreeps: !creep.pos.inRangeTo(target, 3)});
            }
        }
	}
};

module.exports = roleScavenger;
