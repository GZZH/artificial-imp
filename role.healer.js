var roleHealer = {
    /** @param {Creep} creep **/
	goto: function(creep, direction) {
	    if(direction){
	        creep.moveTo(direction, {visualizePathStyle: {stroke: '#ffffff'}})
	    }
	},
	heal: function(creep, target) {
	    if(target){
	        if(creep.pos.isNearTo(target)){
	            creep.heal(target)
	        }
	        else if(creep.rangedHeal(target) == ERR_NOT_IN_RANGE || !creep.pos.isNearTo(target)){
	            creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}});
	        }
	    }
	},
	sentinel: function(creep) {
	    var target = creep.pos.findClosestByRange(FIND_CREEPS, {
            filter: (dammaged_creep) => dammaged_creep.my && dammaged_creep.hits < dammaged_creep.hitsMax
        });
        if(target){
            if(creep.pos.isNearTo(target)){
                creep.heal(target)
            }
            else if (creep.rangedHeal(target) == ERR_NOT_IN_RANGE || !creep.pos.isNearTo(target)){
                creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}});
            }
            return true
        }
        else{
            return false
        }
	}
};

module.exports = roleHealer;