var roleHarvester = require('role.harvester');
var roleUpgrader = require('role.upgrader');
var roleBuilder = require('role.builder');
var roleCarryer = require('role.carryer');
var roleCharger = require('role.charger');
var roleFixer = require('role.fixer');
var roleScavenger = require('role.scavenger');
var roleLongDisHarvester = require('role.longDisHarvester');

var roleHealer = require('role.healer');
var rolereserver = require('role.reserver');
var roleCloserange = require('role.closerange')
var roleLongrange = require('role.longrange')

var Scratchpaper = require('Scratchpaper');
var resourceManager = require('resource.manager');

var warband = require('role.warband');

var usedCpu = 0;

function loadWarbands(){
    /*
    return = [warband]
    */
    var cacheWarbands = []
    for (var num = 0; num < Object.keys(Memory.Warbands).length; num++){
        var key = Object.keys(Memory.Warbands)[num];
        cacheWarbands.push(new warband(num, Memory.Warbands[key].MeetPoint, Memory.Warbands[key].AttackPoint));
    }
    return cacheWarbands
}

function loadCreepDict(){
    /*
    return = {'civilian':{room:{role:[creep]}},
              'warband':{warband:{role:[creep]}}}
    */
    var cacheCreepDict = {'civilian':{}, 'warband':{}}
    for(var name in Game.creeps) {
        var creep = Game.creeps[name];
        if(creep.memory.belonging == 'civilian'){
            if(!(creep.memory.resetPos.roomName in cacheCreepDict['civilian'])){
                cacheCreepDict['civilian'][creep.memory.resetPos.roomName] = {};
            }
            if(!(creep.memory.role in cacheCreepDict['civilian'][creep.memory.resetPos.roomName])){
                cacheCreepDict['civilian'][creep.memory.resetPos.roomName][creep.memory.role] = [];
            }
            cacheCreepDict['civilian'][creep.memory.resetPos.roomName][creep.memory.role].push(creep);
        }
        else{
            if(!(creep.memory.belonging in cacheCreepDict['warband'])){
                cacheCreepDict['warband'][creep.memory.belonging] = {};
            }
            if(!(creep.memory.role in cacheCreepDict['warband'][creep.memory.belonging])){
                cacheCreepDict['warband'][creep.memory.belonging][creep.memory.role] = [];
            }
            cacheCreepDict['warband'][creep.memory.belonging][creep.memory.role].push(creep);
        }
    }
    return cacheCreepDict
}

function cleanCreep(){
    for(var name in Memory['creeps']) {
        if(!Game.creeps[name]) {
            delete Memory.creeps[name];
            console.log('Clearing non-existing creep memory:', name);
        }
    }
}
function updateFixList(){
    for(var roomidx in Game.rooms){
        var room = Game.rooms[roomidx];
        if(room && room.controller.my && !(room.name in Memory.damagedStructuresDict)){
            Memory.damagedStructuresDict[room.name] = {'keys':[],'values':[]};
        }
    }
    for(var roomName in Memory.damagedStructuresDict){
        var room = Game.rooms[roomName];
        var damagedStructures = room.find(FIND_STRUCTURES, {
            filter: (structure) => structure.hits < structure.hitsMax-800 &&
            structure.structureType != STRUCTURE_WALL &&
            structure.structureType != STRUCTURE_RAMPART
        });
        if(!damagedStructures.length)
        {
            var damagedStructures = room.find(FIND_STRUCTURES, {
                filter: (structure) => structure.hits < 500000 &&
                    (structure.structureType == STRUCTURE_WALL ||
                    structure.structureType == STRUCTURE_RAMPART)
            });
        }
        for (var idx in damagedStructures){
            var damagedPrecent = damagedStructures[idx].hits / damagedStructures[idx].hitsMax;
            var cacheStructuresDict = {'keys':[],'values':[]};
            var added = false;
            var dictSize = 10;
            for (var jdx in Memory.damagedStructuresDict[roomName]['keys']){
                if(cacheStructuresDict['keys'].length < dictSize)
                {
                    if(!added && damagedPrecent < Memory.damagedStructuresDict[roomName]['values'][jdx]){
                        cacheStructuresDict['keys'].push(damagedStructures[idx].id);
                        cacheStructuresDict['values'].push(damagedPrecent);
                        added = true;
                    }
                    else{
                        cacheStructuresDict['keys'].push(Memory.damagedStructuresDict[roomName]['keys'][jdx]);
                        cacheStructuresDict['values'].push(Memory.damagedStructuresDict[roomName]['values'][jdx]);
                    }
                }
            }
            if(!added && cacheStructuresDict['keys'].length < dictSize){
                cacheStructuresDict['keys'].push(damagedStructures[idx].id);
                cacheStructuresDict['values'].push(damagedPrecent);
            }
            Memory.damagedStructuresDict[roomName] = cacheStructuresDict;
        }
    }
}
function runTower(){
    //Memory.damagedStructuresDict = {};
    var towers = _.filter(Game.structures, (structure) => structure.structureType == STRUCTURE_TOWER);
    for(var idx in towers) {
        var tower = towers[idx];
        var closestHostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
        var damagedCreeps = tower.pos.findClosestByRange(FIND_CREEPS, {
            filter: (dammaged_creep) => dammaged_creep.my && dammaged_creep.hits < dammaged_creep.hitsMax
        });
        if(closestHostile) {
            tower.attack(closestHostile);
        }
        else if(damagedCreeps){
            tower.heal(damagedCreeps);
        }
        else if(tower.energy > 4*tower.energyCapacity/5){
            var target = Game.getObjectById(Memory.damagedStructuresDict[tower.room.name]['keys'].shift());
            var targetDamagedPrecent = Memory.damagedStructuresDict[tower.room.name]['values'].shift()
            if(target){
                console.log(target.pos,'damage precent:',targetDamagedPrecent * 100 ,'%')
                tower.repair(target);
            }
        }
    }
}

function getWarbandRequests(creepDict){
    /*
    return = [{'role':role, 'composition':composition, 'belonging':belonging}]
    */
    var cacheWarbandRequests = []
    for(var warband in Memory.Warbands) {
        var warbandSetup = Memory.Warbands[warband]['Composition'];
        var warbandCreeps = null
        if(warband in creepDict['warband']){
            warbandCreeps = creepDict['warband'][warband]
        }
        for(var role in warbandSetup){
            if(warbandSetup[role]['number'] != 0){
                //console.log(warbandCreeps[role].length)
                while(!warbandCreeps || !(role in warbandCreeps) || warbandCreeps[role].length < warbandSetup[role]['number']){
                    cacheWarbandRequests.push({'role':role, 'composition':warbandSetup[role]['composition'],'belonging':warband})
                    if(!warbandCreeps){
                        creepDict['warband'][warband] = {}
                        warbandCreeps = creepDict['warband'][warband]
                    }
                    if(!(role in warbandCreeps)){
                        creepDict['warband'][warband][role] = []
                        warbandCreeps = creepDict['warband'][warband]
                    }
                    creepDict['warband'][warband][role].push(null)
                    warbandCreeps = creepDict['warband'][warband]
                }
            }
        }
    }
    return cacheWarbandRequests
}
 function ShowUsedCPU(){
     console.log('cpu cost', Game.cpu.getUsed() - usedCpu );
     usedCpu = Game.cpu.getUsed();
 }
module.exports.loop = function () {
    console.log(' ');
    console.log('-------------------------------------------------');
    console.log('CPU bucket remain: ', Game.cpu.bucket)
    usedCpu = Game.cpu.getUsed();
    //Scratchpaper.run();
    //Scratchpaper.test2();
    cleanCreep();
    ShowUsedCPU()

    resourceManager.update();
    ShowUsedCPU()

    var warbands = loadWarbands(); //[warband]
    ShowUsedCPU()

    var creepDict = loadCreepDict(); //{'civilian':{room:{role:[creep]}},
                                     // 'warband':{warband:{role:[creep]}}}
    ShowUsedCPU()

    var warbandRequests = getWarbandRequests(creepDict); //[{'role':role, 'composition':composition, 'belonging':belonging}]
    ShowUsedCPU()

    updateFixList();
    ShowUsedCPU()

    runTower();
    ShowUsedCPU()

    var moduleDict = {'harvester':roleHarvester,
                      'carryer':roleCarryer,
                      'charger':roleCharger,
                      'upgrader':roleUpgrader,
                      'fixer':roleFixer,
                      'builder':roleBuilder,
                      'scavenger':roleScavenger}
    var partDict = {'MOVE': MOVE,
                    'WORK': WORK,
                    'CARRY': CARRY,
                    'ATTACK': ATTACK,
                    'RANGED_ATTACK': RANGED_ATTACK,
                    'HEAL': HEAL,
                    'CLAIM': CLAIM,
                    'TOUGH': TOUGH}
    var costDict = {'MOVE': 50,
                    'WORK': 100,
                    'CARRY': 50,
                    'ATTACK': 80,
                    'RANGED_ATTACK': 150,
                    'HEAL': 250,
                    'CLAIM': 600,
                    'TOUGH': 10}
    //console.log("?????????????????",Object.keys(Game.creeps).length)
    for(var roomidx in Game.rooms){
        var room = Game.rooms[roomidx];
        console.log('---------',room,'---------');
        if(room.storage){
            console.log('Energy storage:',room.storage.store[RESOURCE_ENERGY])
        }
        else{
            console.log('Energy storage: null')
        }
        var roomEnergy = room.energyAvailable;
        console.log('Energy in spawn:',roomEnergy)
        var emergency = room.find(FIND_HOSTILE_CREEPS).length > 0;
        var roomSetup = null;
        var requestList = []//[{'role':role, 'composition':composition, 'belonging':belonging}]
        if(room.name in Memory.RoomSetup){
            roomSetup = Memory.RoomSetup[room.name];
        }
        if(roomSetup){
            var roomCreeps = null
            if(room.name in creepDict['civilian']){
                roomCreeps = creepDict['civilian'][room.name]
            }
            for(var role in roomSetup['totalSetup']){
                if(roomSetup['totalSetup'][role]['number'] != 0){
                    var creepCost = 0;
                    for(var idx in roomSetup['totalSetup'][role]['composition']){
                        creepCost += costDict[roomSetup['totalSetup'][role]['composition'][idx]];
                    }
                    console.log('Standard',role,'costs',creepCost, 'with',roomEnergy,'in room')
                    while(creepCost <= roomEnergy && (!roomCreeps || !(role in roomCreeps) || roomCreeps[role].length < roomSetup['totalSetup'][role]['number'])){
                        requestList.push({'role':role, 'composition':roomSetup['totalSetup'][role]['composition'],'belonging':'civilian'})
                        if(!roomCreeps){
                            creepDict['civilian'][room.name] = {}
                            roomCreeps = creepDict['civilian'][room.name]
                        }
                        if(!(role in roomCreeps)){
                            creepDict['civilian'][room.name][role] = []
                            roomCreeps = creepDict['civilian'][room.name]
                        }
                        creepDict['civilian'][room.name][role].push(null)
                        roomCreeps = creepDict['civilian'][room.name]
                    }
                }
            }
            for(var role in roomSetup['minSetup']){
                //console.log(room.name in creepDict['civilian'])
                if(roomSetup['minSetup'][role]['number'] != 0){
                    var creepCost = 0;
                    for(var idx in roomSetup['minSetup'][role]['composition']){
                        creepCost += costDict[roomSetup['minSetup'][role]['composition'][idx]];
                    }
                    console.log('Min',role,'costs',creepCost, 'with',roomEnergy,'in room')
                    while(creepCost <= roomEnergy && (!roomCreeps || !(role in roomCreeps) || roomCreeps[role].length < roomSetup['minSetup'][role]['number'])){
                        requestList.push({'role':role, 'composition':roomSetup['minSetup'][role]['composition'],'belonging':'civilian'})
                        if(!roomCreeps){
                            creepDict['civilian'][room.name] = {}
                            roomCreeps = creepDict['civilian'][room.name]
                        }
                        if(!(role in roomCreeps)){
                            creepDict['civilian'][room.name][role] = []
                            roomCreeps = creepDict['civilian'][room.name]
                        }
                        creepDict['civilian'][room.name][role].push(null)
                        roomCreeps = creepDict['civilian'][room.name]
                    }
                }
            }
        }
        //console.log(room.name,room.name in Memory.RoomSetup)

        var storage = room.storage;
        var storageLink = null;
        var links = [];
        var upgradeLink = room.controller.pos.findClosestByRange(FIND_MY_STRUCTURES, {filter: (structure) => {return structure.structureType == STRUCTURE_LINK &&
                                                                                                                      structure.pos.inRangeTo(room.controller.pos, 3);}});

        if(storage){
            storageLink = storage.pos.findClosestByRange(FIND_STRUCTURES, {filter: (structure) => {return structure.structureType == STRUCTURE_LINK;}});
            links = storage.room.find(FIND_STRUCTURES, {filter: (structure) => {return (structure.structureType == STRUCTURE_LINK) &&
                                                                                           (structure.id != storageLink.id) &&
                                                                                           (!upgradeLink || structure.id != upgradeLink.id);}});
        }
        var toUpgrade = (upgradeLink && upgradeLink.store[RESOURCE_ENERGY] < upgradeLink.store.getCapacity(RESOURCE_ENERGY)/2);
        for(var idx in links){
            var link = links[idx];
            //console.log(link)
            if(toUpgrade){
                link.transferEnergy(upgradeLink);
            }else{
                link.transferEnergy(storageLink);
            }
        }

        /*var sources = Game.rooms['W44S8'].find(FIND_SOURCES);

        for (var source in sources) {
            var has_source = false;
            for (var c_source in Memory.SourcesID)
            {
                if (!has_source && sources[source].id == Memory.SourcesID[c_source]){
                    has_source = true;
                }
            }
            if(!has_source){
                Memory.SourcesID.push(sources[source].id);
            }
        }*/

        var roomSpawns = room.find(FIND_MY_SPAWNS);
        for(var idx in roomSpawns){
            var roomSpawn = roomSpawns[idx];
            var spawning = roomSpawn.spawning;
            var request = requestList.shift();
            if(emergency){
              roomSpawn.room.visual.text(
                  '🚨 Invaders',
                  roomSpawn.pos.x - 1,
                  roomSpawn.pos.y,
                  {align: 'right', opacity: 0.8});
            }
            if(spawning) {
                var spawningCreep = Game.creeps[spawning.name];
                roomSpawn.room.visual.text(
                    '🛠️' + spawningCreep.memory.role,
                    roomSpawn.pos.x + 1,
                    roomSpawn.pos.y,
                    {align: 'left', opacity: 0.8});
            }
            if(!roomSpawn.spawning)
            {
                if(!request && warbandRequests.length && room.name == 'W44S8')
                {
                    var creepCost = 0;
                    var cacheRequest = warbandRequests[0];
                    for(var idx in cacheRequest.composition){
                        creepCost += costDict[cacheRequest.composition[idx]];
                    }
                    if(creepCost <= roomEnergy){
                        request = warbandRequests.shift();
                    }
                    console.log(cacheRequest.belonging , cacheRequest.role,'costs',creepCost, 'with',roomEnergy,'in room')
                }
                if(request)
                {
                    var newName = request.role + Game.time;
                    console.log('Spawning new ' + request.role + ': '+ newName);
                    var cacheParts = []
                    for(var idx in request.composition){
                        cacheParts.push(partDict[request.composition[idx]])
                    }
                    roomSpawn.spawnCreep(cacheParts, newName,
                        {memory: {role: request.role,
                                belonging : request.belonging,
                                resetPos: roomSpawn.pos}});
                }
            }
        }
    }
    ShowUsedCPU()

    for(var room in creepDict['civilian']){
        for (var role in creepDict['civilian'][room]){
            for (var idx in creepDict['civilian'][room][role]){
                var cacheCreep = creepDict['civilian'][room][role][idx];
                if (cacheCreep){
                    moduleDict[role].run(cacheCreep);
                }
            }
        }
    }
    ShowUsedCPU()

    for(var idx in warbands){
        var warband = warbands[idx]
        var wholeWarband = [];
        if (warband.name() in creepDict['warband']){
            for (var role in creepDict['warband'][warband.name()]){
                for (var idx in creepDict['warband'][warband.name()][role]){
                    var cacheCreep = creepDict['warband'][warband.name()][role][idx];
                    if (cacheCreep){
                        wholeWarband.push(cacheCreep);
                    }
                }
            }
            warband.run(wholeWarband);
        }
    }
    //resourceManager.printAll();
    ShowUsedCPU()
}
