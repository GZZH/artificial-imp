var roleCarryer = require('role.carryer');
var resourceManager = require('resource.manager');

var roleFixer = {

    /** @param {Creep} creep **/
    run: function(creep) {

	    if(creep.memory.fixing && creep.carry.energy == 0) {
            creep.memory.fixing = false;
            creep.say('🔄 charge');
	    }
	    if(!creep.memory.fixing && creep.carry.energy == creep.carryCapacity) {
	        creep.memory.fixing = true;
	        creep.say('🚧 fixing');
	    }

	    if(creep.memory.fixing) {
            var target = Game.getObjectById(Memory.damagedStructuresDict[creep.room.name]['keys'].shift());
            var targetDamagedPrecent = Memory.damagedStructuresDict[creep.room.name]['values'].shift()
            if(target){
                console.log(target.pos,'damage precent:',targetDamagedPrecent * 100 ,'%')
                if (creep.repair(target) == ERR_NOT_IN_RANGE || !creep.pos.isNearTo(target)){
                    creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms:1, ignoreCreeps: false && !creep.pos.inRangeTo(target, 7)});
                    target = creep.pos.findClosestByRange(FIND_STRUCTURES,{
                            filter: (structure) => {return structure.hits < structure.hitsMax &&
                                                           creep.pos.inRangeTo(structure.pos, 3);}});
                    if(target) {
                        creep.repair(target)
                    }
                }
            }
            else{
                roleCarryer.run(creep);
            }
	    }
	    else {
	        var supply = null;
            //console.log('--------------------------\n')
            for(var idx in Memory.ProduceContainersID){
                var cache_target = Game.getObjectById(Memory.ProduceContainersID[idx]);
                //console.log(creep.name, ': ',  cache_target, cache_target.store[RESOURCE_ENERGY]);
                if(cache_target && resourceManager.getEstimate(cache_target.id) > 0 &&
                  (supply == null ||
                   creep.pos.getRangeTo(supply) > creep.pos.getRangeTo(cache_target))){
                    supply = cache_target;
                    //console.log(creep.name, 'change to: ',  cache_target);
                }
                if(cache_target && resourceManager.getEstimate(cache_target.id) >= cache_target.storeCapacity){
                    supply = cache_target;
                    break;
                }
            }
            if(!supply){
                var supply = creep.room.storage
            }
            //console.log(creep.name, 'final: ',  supply, supply.store[RESOURCE_ENERGY]);
            //console.log(creep.name, 'load from: ', destination);
            if(supply){
                resourceManager.changeEstimate(supply.id, -(creep.carryCapacity - creep.carry.energy), supply.room.name);
                if(creep.withdraw(supply,RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(supply, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms:1, ignoreCreeps: !creep.pos.inRangeTo(supply, 5)})
                }
            }
	    }

        if(creep.room.name != creep.memory.resetPos.roomName){
            creep.moveTo(creep.memory.resetPos, {visualizePathStyle: {stroke: '#ffffff'}, ignoreCreeps: true});
        }
	}
};

module.exports = roleFixer;
