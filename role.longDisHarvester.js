var resourceManager = require('resource.manager');
var roleBuilder = require('role.builder');

var roleExpeditionHarvester = {

    /** @param {Creep} creep **/
    state: function(creep) {
        if(!creep.memory.harvesting && _.sum(creep.carry) == 0) {
            creep.memory.harvesting = true;
            creep.say('⛏️ harvest');
	    }
	    if(creep.memory.harvesting && _.sum(creep.carry) == creep.carryCapacity && !creep.memory.otherwork) {
	        creep.memory.harvesting = false;
	        creep.say('🔄 unload');
	    }
	},
	goto: function(creep, direction) {
	    if(direction){
	        creep.moveTo(direction, {visualizePathStyle: {stroke: '#ffffff'}})
	    }
	},
	changeJob: function(creep) {
	    roleBuilder.run(creep);
	},
	build: function(creep, flag){
        if(creep.carry.energy == 0)
        {
            return false;
        }
	    var target = creep.pos.findClosestByRange(FIND_CONSTRUCTION_SITES,{
                filter: (structure) => {return structure.my &&
                                               creep.pos.inRangeTo(structure.pos, 3);}});
        if(target) {
            creep.build(target)
            return true
        }
        else{
            return false// disable the building function by that

            target = creep.pos.findClosestByRange(FIND_CONSTRUCTION_SITES,{
                filter: (structure) => {return structure.my && structure.room == flag.room;}});
            if(target){
                if(creep.build(target) == ERR_NOT_IN_RANGE && !creep.pos.isNearTo(target)) {
                    creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}});
                }
                return true
            }
            else{
                return false
            }
        }
	},
	fix: function(creep){
        if(creep.carry.energy == 0)
        {
            return false;
        }
	    var target = creep.pos.findClosestByRange(FIND_STRUCTURES,{
                filter: (structure) => {return structure.hits < structure.hitsMax &&
                                               creep.pos.inRangeTo(structure.pos, 3);}});
        if(target) {
            creep.repair(target)
            return true
        }
        else{
            return false
        }
	},
	upgrade: function(creep){
	    var target = creep.room.controller
        if(target && target.my && creep.pos.inRangeTo(target.pos, 3)) {
            creep.upgradeController(target)
            return true
        }
        else{
            return false
        }
	},
	scavenge: function(creep, flag){
        if(!flag)
        {
            return false
        }
        if(!creep.memory.harvesting){
	    //if(_.sum(creep.carry) == creep.carryCapacity){
	        return false
	    }
        var target = creep.pos.findClosestByRange(FIND_DROPPED_RESOURCES,{
                filter: (source) => {return creep.pos.inRangeTo(source.pos, 50) && flag.room == creep.room;}});
        var tombstone = creep.pos.findClosestByRange(FIND_TOMBSTONES, {
            filter: (tombstone) => _.sum(tombstone.store) > 0 &&
                                   creep.pos.inRangeTo(tombstone.pos, 50) &&
                                   flag.room == creep.room
        });
        var containers = creep.pos.findClosestByRange(FIND_STRUCTURES, {
            filter: (structure) => _.sum(structure.store) > 0 &&
                                   structure.structureType == STRUCTURE_CONTAINER &&
                                   flag.pos.inRangeTo(structure.pos, 3) &&
                                   flag.room == creep.room
        });
        if(target) {
            if(creep.pickup(target) == ERR_NOT_IN_RANGE) {
                creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms:1});
            }
            return true
        }
        else if(tombstone){
            var key = null;
            for(var k in tombstone.store){
                if(!key && tombstone.store[k] > 0){
                    key = k;
                }
            }
            if(key && creep.withdraw(tombstone, key) == ERR_NOT_IN_RANGE) {
                creep.moveTo(tombstone, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms:1});
            }
            return true
        }
        else if(containers){
            var key = null
            for(var k in containers.store){
                if(containers.store[k] > 0){
                    key = k;
                }
            }
            if(creep.withdraw(containers, key) == ERR_NOT_IN_RANGE) {
                creep.moveTo(containers, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms:1});
            }
            return true
        }
        return false
	},
	harvest: function(creep, flag, range = 10) {
	    if(flag.room){
	        var target = flag.pos.findClosestByRange(FIND_SOURCES, {
                    filter: (source) => {
                        return (flag.pos.inRangeTo(source, range));
                    }
            });
	    }
	    else{
            return false
	    }
        //console.log(creep,'pass')
	    if(target){
	        creep.memory.otherwork = false;
	        if(creep.harvest(target) == ERR_NOT_IN_RANGE || !creep.pos.isNearTo(target)) {
                    creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}});
            }
            return true
	    }
	    creep.memory.otherwork = true;
	    return false
	},
	unload: function(creep, flag, range = 20) {
	    //console.log(creep)
	    if(flag.room){
	        if(_.sum(creep.carry) != creep.carry.energy && flag.room.storage){
	            var target = flag.room.storage;
	        }
	        else{
                var tower = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                        filter: (structure) => {
                            return (structure.structureType == STRUCTURE_TOWER &&
                                    resourceManager.getEstimate(structure.id) < structure.energyCapacity &&
                                    creep.pos.inRangeTo(structure, 1));
                        }
                });
                creep.transfer(tower, RESOURCE_ENERGY)
                var target = flag.pos.findClosestByRange(FIND_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType == STRUCTURE_LINK &&
                            structure.energy < structure.energyCapacity &&
                            flag.pos.inRangeTo(structure, 5)) ;
                        }
                    });
                if(!target){
        	        target = flag.pos.findClosestByRange(FIND_STRUCTURES, {
                            filter: (structure) => {
                                return ((((structure.structureType == STRUCTURE_EXTENSION ||
                                           structure.structureType == STRUCTURE_SPAWN ||
                                           structure.structureType == STRUCTURE_TOWER) &&
                                          structure.energy < structure.energyCapacity)||
                                          ((structure.structureType == STRUCTURE_CONTAINER ||
                                          structure.structureType == STRUCTURE_TERMINAL) &&
                                           structure.store[RESOURCE_ENERGY] < structure.storeCapacity)) &&
                                         flag.pos.inRangeTo(structure, range));
                            }
                    });
                }
	        }
	    }
	    else{
	        return false
	    }
        if(target){
            if(target.room == creep.room){
                resourceManager.changeEstimate(target.id, creep.carry.energy, target.room.name);
            }
            else{
                if(!this.fix(creep) && !this.build(creep,flag)){
                    this.upgrade(creep)
                }
            }
            var key = null;
            for(var k in creep.carry){
                if(creep.carry[k] > 0){
                    key = k;
                }
            }
            if(creep.transfer(target, key) == ERR_NOT_IN_RANGE || !creep.pos.isNearTo(target)) {
                    creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}});
            }
            return true
	    }
        else{
            var target = creep.room.controller;
            if(target && target.my &&target.pos.inRangeTo(flag.pos, 2))
            {
                if(creep.upgradeController(target) == ERR_NOT_IN_RANGE || !creep.pos.isNearTo(target)) {
                        creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}});
                }
                return true;
            }
        }
	    return false
	}
};

module.exports = roleExpeditionHarvester;
