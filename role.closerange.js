var roleCloserange = {
    /** @param {Creep} creep **/
	goto: function(creep, direction) {
	    if(direction){
	        creep.moveTo(direction, {visualizePathStyle: {stroke: '#ffffff'}, ignoreCreeps: !creep.pos.inRangeTo(direction, 5)})
	    }
	},
	attack: function(creep, target) {
	    if(target){
	        if(creep.attack(target) == ERR_NOT_IN_RANGE || !creep.pos.isNearTo(target)) {
                    creep.moveTo(target, {visualizePathStyle: {stroke: '#ffffff'}, ignoreCreeps: !creep.pos.inRangeTo(target, 5)});
            }
	    }
	},
	sentinel: function(creep) {
	    var closestHostile = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS,{
                        filter: (hostile) => {return (Memory.WhiteList.indexOf(hostile.owner.username) == -1 );}})
	    var closestStructure = creep.pos.findClosestByPath(FIND_HOSTILE_STRUCTURES, {
                                filter: (structure) => {return (structure.structureType != STRUCTURE_CONTROLLER);}
                            })
        if(closestHostile) {
            if(creep.attack(closestHostile) == ERR_NOT_IN_RANGE || !creep.pos.isNearTo(closestHostile)) {
                    creep.moveTo(closestHostile, {visualizePathStyle: {stroke: '#ffffff'},ignoreCreeps: !creep.pos.inRangeTo(closestHostile, 5)});
            }
            return true
        }
        else if(closestStructure) {
            if(creep.attack(closestStructure) == ERR_NOT_IN_RANGE || !creep.pos.isNearTo(closestStructure)) {
                    creep.moveTo(closestStructure, {visualizePathStyle: {stroke: '#ffffff'},ignoreCreeps: !creep.pos.inRangeTo(closestStructure, 5)});
            }
            return true
        }
        else{
            return false
        }
	}
};

module.exports = roleCloserange;