var resourceManager = require('resource.manager');

var roleCarryer = {
    /** @param {Creep} creep **/
    run: function(creep) {
        //console.log(creep.name, ' Time: ', Game.time);
        if(creep.memory.unloading && _.sum(creep.carry) == 0) {
            creep.memory.unloading = false;
            creep.say('🔄 load');
	    }
	    if(!creep.memory.unloading && _.sum(creep.store) == creep.store.getCapacity()) {
	        creep.memory.unloading = true;
	        creep.say('🔄 unload');
	    }

        if(creep.memory.unloading) {
            var destination = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                filter: (structure) => {return (structure.structureType == STRUCTURE_TOWER && resourceManager.getEstimate(structure.id) < structure.energyCapacity);
                }
            });

            var cache_destination = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                filter: (structure) => {return ((structure.structureType == STRUCTURE_EXTENSION ||
                                                structure.structureType == STRUCTURE_SPAWN) && resourceManager.getEstimate(structure.id) < structure.energyCapacity);
                }
            });
            /*var destination = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                filter: (structure) => {return ((structure.structureType == STRUCTURE_TOWER ||
                                                 structure.structureType == STRUCTURE_EXTENSION ||
                                                 structure.structureType == STRUCTURE_SPAWN) && resourceManager.getEstimate(structure.id) < structure.energyCapacity);
                }
            });*/


            if(_.sum(creep.carry) != creep.carry.energy && creep.room.storage){
                destination = creep.room.storage;
            }
            else if(cache_destination){
                destination = cache_destination;
            }
            else{
                var empty_consumer = null;
                var cloest_consumer = null;
                for(var idx in Memory.SecondaryContainersID){
                    var cache_target = Game.getObjectById(Memory.SecondaryContainersID[idx]);
                    if (cache_target &&
                        resourceManager.getEstimate(cache_target.id) <= 0.5 * cache_target.storeCapacity  &&
                        (empty_consumer == null ||
                        (creep.pos.getRangeTo(empty_consumer) > creep.pos.getRangeTo(cache_target)))){
                        empty_consumer = cache_target;
                    }
                    if (cache_target &&
                        resourceManager.getEstimate(cache_target.id) < cache_target.storeCapacity &&
                       (!cloest_consumer ||
                       creep.pos.getRangeTo(cloest_consumer) > creep.pos.getRangeTo(cache_target))){
                        cloest_consumer = cache_target;
                    }
                }
                if(empty_consumer)
                {
                    destination = empty_consumer;
                }
                else if(cloest_consumer)
                {
                    destination = cloest_consumer;
                }
                else if(!destination){
                    destination = creep.room.storage;
                }
            }

            /*if(destination && cloest_consumer && creep.pos.getRangeTo(destination) > creep.pos.getRangeTo(cloest_consumer))
            {
                destination = cloest_consumer;
            }*/

            //console.log(creep.name, 'unload to: ', destination);
            if(destination){
                var key = null;
                for(var k in creep.store){
                    if(creep.store[k] > 0){
                        key = k;
                    }
                }
                if(key == RESOURCE_ENERGY){
                    resourceManager.changeEstimate(destination.id, creep.store[RESOURCE_ENERGY],destination.room.name);
                }
                if(creep.transfer(destination, key) == ERR_NOT_IN_RANGE || !creep.pos.isNearTo(destination)) {
                    creep.moveTo(destination, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms:1});
                }
            }
        }
        else {
            var supply = null;
            var key = null;
            //console.log('--------------------------\n')
            supply = creep.pos.findClosestByRange(FIND_STRUCTURES, {
                filter: (structure) => {return ((structure.structureType == STRUCTURE_CONTAINER || structure.structureType == STRUCTURE_TERMINAL )&&
                                                !Memory.SecondaryContainersID.includes(structure.id) &&
                                                (resourceManager.getEstimate(structure.id) > 0 || _.sum(structure.store) > structure.store[RESOURCE_ENERGY]));
                }
            });
            /*for(var target in targets){
                console.log('testing11111111111111111111',targets);
                var cache_target = target;
                //console.log(creep.name, ': ',  cache_target, resourceManager.getEstimate(cache_target.id));
                if(resourceManager.getEstimate(cache_target.id) > 0 &&
                   (supply == null ||
                    creep.pos.getRangeTo(supply) > creep.pos.getRangeTo(cache_target))){
                    supply = cache_target;
                    //console.log(creep.name, 'change to: ',  cache_target);
                }
                if(resourceManager.getEstimate(cache_target.id) >= cache_target.storeCapacity){
                    supply = cache_target;
                    break;
                }
            }*/
            if(!supply){
                supply = creep.room.storage;
                key = RESOURCE_ENERGY;
            }
            //console.log(creep.name, 'final: ',  supply, supply.store[RESOURCE_ENERGY]);
            //console.log(creep.name, 'load from: ', destination);
            if(supply){
                if(!key)
                {
                    for(var k in supply.store){
                        if(supply.store[k] > 0){
                            key = k;
                            break;
                        }
                    }
                }
                if(key == RESOURCE_ENERGY){
                    resourceManager.changeEstimate(supply.id, -(creep.store.getCapacity() - _.sum(creep.store)), supply.room.name);
                }
                if(creep.withdraw(supply, key) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(supply, {visualizePathStyle: {stroke: '#ffffff'}, maxRooms:1});
                }
            }
        }
        if(creep.room.name != creep.memory.resetPos.roomName){
            creep.moveTo(creep.memory.resetPos, {visualizePathStyle: {stroke: '#ffffff'}, ignoreCreeps: true});
        }
	}
};

module.exports = roleCarryer;
